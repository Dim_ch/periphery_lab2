#include <Windows.h>
#include <WinUser.h>
#include <Shlwapi.h>
#include <PathCch.h>
#include <locale.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <wchar.h>
//#include <process.h>
#include <tlhelp32.h>

HWND explorer;

// ������� PrintProgramInfo
// ������� ���������� � �������� ���������.
void PrintProgramInfo() {
	_tprintf(TEXT("W: ������ ��������� ���������.\n"));
	_tprintf(TEXT("F: �������� ���� � �����.\n"));
	_tprintf(TEXT("w: �������� ���������� �����.\n"));
	_tprintf(TEXT("H: �������� �����.\n"));
	_tprintf(TEXT("Y: �������� ����������.\n"));
	_tprintf(TEXT("q: ����� �� ���������.\n"));
}

LPCTSTR ErrorMessage(DWORD error)
{
	LPVOID lpMsgBuf;

	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
		| FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);

	return((LPCTSTR)lpMsgBuf);
}

//  ������� PrintError
//  ������� ���������� �� ������.
void PrintError(LPCTSTR errDesc)
{
	LPCTSTR errMsg = ErrorMessage(GetLastError());
	_tprintf(TEXT("\n** ������ ** %s: %s\n"), errDesc, errMsg);
	LocalFree((LPVOID)errMsg);
}

// ������� InputPath
// ��������� � ���������� ������ � ���������� 0, ���� ������ �������� ���� � ����������.
BOOL InputPath(TCHAR *OriginalPath) {
	TCHAR OutPath[MAX_PATH] = { NULL };
	TCHAR path[MAX_PATH] = { NULL };
	BOOL bRet = 1;

	_getws_s(path, MAX_PATH);

	if (0 != wcscpy_s(OutPath, MAX_PATH, OriginalPath)) return bRet;

	if (0 == _tcsncmp(path, TEXT("..\\"), 3) || 
		0 == _tcsncmp(path, TEXT(".\\"), 2))
		if (S_OK == PathCchAppend(OutPath, MAX_PATH, path)) {
			SecureZeroMemory(path, sizeof(TCHAR) * MAX_PATH);
			if (S_OK != PathCchCanonicalize(path, MAX_PATH, OutPath))
				bRet = -1;
		}
		else
			bRet = -1;

	if (-1 == bRet)
		return bRet;

	if (PathIsDirectory(path) & FILE_ATTRIBUTE_DIRECTORY) {
		SecureZeroMemory(OriginalPath, sizeof(TCHAR) * MAX_PATH);
		bRet = wcscpy_s(OriginalPath, MAX_PATH, path);
	}

	return bRet;
}

// ������� CreateTextFile.
// ������ ���� � ���������� 0 � ������ ������.
BOOL CreateTextFile() {
	TCHAR FileName[MAX_PATH] = { NULL };
	HANDLE hFile = INVALID_HANDLE_VALUE;

	_getws_s(FileName, MAX_PATH);

	hFile = CreateFile(FileName,
		GENERIC_READ |
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_NEW, 
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		PrintError(TEXT("�� ������� ������� ����\n"));
		return 1;
	}

	if (!CloseHandle(hFile)) {
		PrintError(TEXT("�� ������� ������� ���������� (hFile)"));
		return 1;
	}

	return 0;
}

void SetCurrentDir(LPCTSTR lpPathName) {
	if (0 == SetCurrentDirectory(lpPathName))
		PrintError(TEXT("�� ������� �������� ������� �������"));
	return;
}

// ������� RemoveFile
// ������� ���� � ���������� 0 � ������ ������.
BOOL RemoveFile() {
	TCHAR FileName[MAX_PATH] = { NULL };
	_getws_s(FileName, MAX_PATH);

	if (0 == DeleteFile(FileName)) {
		PrintError(TEXT("�� ������� ������� ����"));
		return 1;
	}

	return 0;
}

// ������� RunExplorer
// ��������� ��������� � ���������� 0 � ������ ������.
BOOL RunExplorer(STARTUPINFO *lpSi, PROCESS_INFORMATION *lpPi, LPWSTR lpCmdLine) {
	if (!CreateProcess(NULL,   // No module name (use command line)
		lpCmdLine,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		CREATE_NEW_CONSOLE,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		lpSi,            // Pointer to STARTUPINFO structure
		lpPi)           // Pointer to PROCESS_INFORMATION structure
		) {
		PrintError(TEXT("�� ������� ������� ���������"));
		return 1;
	}

	return 0;
}

BOOL CloseExplorer()
{
	HWND hwnd;
	hwnd = FindWindow(TEXT("CabinetWClass"), NULL);

	if (hwnd != NULL)
		SendMessage(hwnd, WM_SYSCOMMAND, SC_CLOSE, 0);
	else
		return 1;

	return 0;
}

// ������� CloseHandleProcess
BOOL CloseHandleProcess(STARTUPINFO * lpsi, PROCESS_INFORMATION * lppi) {
	if (NULL != lppi->hProcess) {
		WaitForSingleObject(lppi->hProcess, INFINITE);
		// Close process and thread handles.
		CloseHandle(lppi->hProcess);
		CloseHandle(lppi->hThread);
		ZeroMemory(lpsi, sizeof(STARTUPINFO));
		lpsi->cb = sizeof(STARTUPINFO);
		ZeroMemory(lppi, sizeof(PROCESS_INFORMATION));
	}
	return 0;
}

int _tmain() {
	_tsetlocale(LC_ALL, TEXT(""));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	PrintProgramInfo();

	int symbol;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	TCHAR Cmdline[] = TEXT("explorer");
	TCHAR CurrentPath[MAX_PATH] = { NULL };

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	GetCurrentDirectory(sizeof(CurrentPath), CurrentPath);

	while ((symbol = _getch()) != 'q') {
		_tprintf(TEXT("%c - %o\n"), symbol, symbol);
		
		switch (symbol)
		{
		case 0127://W
			// ������ ��������� ���������.
			if (NULL == pi.hProcess) {
				_tprintf(TEXT("������ ��������� ���������\n"));
				RunExplorer(&si, &pi, Cmdline);
			}
			else
				_tprintf(TEXT("��������� ��� �������\n"));
			break;

		case 0106://F
			// �������� ���� � �����.
			_tprintf(TEXT("������� ���� � ��������:\n"));
			_tprintf(TEXT("������� �������: %s\n"), CurrentPath);
			if (InputPath(CurrentPath) != 0) {
				_tprintf(TEXT("�� ������� �������� ���� � ��������.\n"));
			}
			else
				SetCurrentDir(CurrentPath);
			_tprintf(TEXT("������� �������: %s\n"), CurrentPath);
			break;

		case 0167://w
			// �������� ���������� �����.
			_tprintf(TEXT("�������� �����\n"));
			_tprintf(TEXT("������� �������� ���������� �����: "));
			if (0 == CreateTextFile())
				_tprintf(TEXT("���� ������� ������.\n"));
			break;

		case 0110://H
			// �������� �����.
			_tprintf(TEXT("������� �������: %s\n"), CurrentPath);
			_tprintf(TEXT("�������� �����\n"));
			_tprintf(TEXT("������� �������� ���������� �����: "));
			if (0 == RemoveFile())
				_tprintf(TEXT("���� �����.\n"));
			break;

		case 0131://Y
			//�������� �������� ���� ����������.
			if (NULL != pi.hProcess) {
				CloseExplorer();
				CloseHandleProcess(&si, &pi);
			}
			else
				_tprintf(TEXT("��������� �� �������\n"));
			break;
		}
	}

	CloseHandleProcess(&si, &pi);
	return 0;
}
